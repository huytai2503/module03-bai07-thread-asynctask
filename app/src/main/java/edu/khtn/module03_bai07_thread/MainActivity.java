package edu.khtn.module03_bai07_thread;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    int[] resList;
    int i1, i2, i3;
    int asyncValue = 0;
    ImageView image;
    TextView textTimer, textRunOnUI, textStatus;
    EditText edtTime;
    ProgressBar progressBar;
    Button btnStart, btnContinue, btnStop;
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                int resImg = (int) msg.obj;
                image.setBackgroundResource(resImg);
            }
            if (msg.what == 2) {
                textTimer.setText(msg.obj.toString());
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        linkViewAndSetListener();
        edtTime.setText("0");
        resList = new int[]{R.drawable.a0, R.drawable.a1,
                R.drawable.a2, R.drawable.a3, R.drawable.a4,
                R.drawable.a5, R.drawable.a6, R.drawable.a7};
        btnStop.setEnabled(false);
        btnContinue.setEnabled(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_start:
                asyncValue = Integer.parseInt(edtTime.getText().toString());
                progressBar.setMax(asyncValue);
                progressBar.setProgress(0);
                btnStop.setEnabled(true);
                btnStart.setEnabled(false);
                btnContinue.setEnabled(false);
                edtTime.setEnabled(false);
                startImageThread();
                startTimerThread();
                startAsyncTask(asyncValue);
                startPostDelay(asyncValue);
                break;
            case R.id.btn_continue:
                asyncValue = Integer.parseInt(edtTime.getText().toString());
                progressBar.setMax(asyncValue);
                progressBar.setProgress(0);
                btnStop.setEnabled(true);
                btnStart.setEnabled(false);
                btnContinue.setEnabled(false);
                edtTime.setEnabled(false);
                startImageThread();
                continueTimerThread();
                startAsyncTask(asyncValue);
                break;
            case R.id.btn_stop:
                Stop();
                break;
        }
    }

    public void postNotify() {
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity
                (this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle("Notify");
        builder.setContentText("Download done");
        builder.setContentIntent(pendingIntent);
        builder.setAutoCancel(true);
        NotificationManager manager = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }

    public void Stop() {
        btnStop.setEnabled(false);
        btnStart.setEnabled(true);
        btnContinue.setEnabled(true);
        edtTime.setEnabled(true);
        i1 = 8;
        i2 = 11;
        i3 = asyncValue + 1;
    }

    public void startPostDelay(int inputValue) {
        textStatus.setText("Running");
        textStatus.postDelayed(new Runnable() {
            @Override
            public void run() {
                textStatus.setText("Finished");
            }
        }, inputValue * 1000);
    }

    public void startRunOnUI() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (i2 % 2 == 0)
                    textRunOnUI.setTextColor(Color.RED);
                else
                    textRunOnUI.setTextColor(Color.BLUE);
            }
        });
    }

    public void startImageThread() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                createImage();
            }
        });
        thread.start();
    }

    public void startTimerThread() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                createTimer(0, 0, 0, 1);
            }
        });
        thread.start();
    }

    public void continueTimerThread() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                String getTimer = textTimer.getText().toString();
                String[] timerArr = getTimer.split(":");
                createTimer(Integer.parseInt(timerArr[0]),
                        Integer.parseInt(timerArr[1]),
                        Integer.parseInt(timerArr[2]), 1);
            }
        });
        thread.start();
    }

    public void createImage() {
        for (i1 = 0; i1 < resList.length; i1++) {
            Message msg = new Message();
            msg.what = 1;
            msg.obj = resList[i1];
            handler.sendMessage(msg);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (i1 == 7)
                i1 = 0;
        }
    }

    public void createTimer(int HH, int MM, int SS, int s) {
        String sHH = "", sMM = "", sSS = "";
        for (i2 = s; i2 < 11; i2++) {
            s = i2;
            if (s == 10) {
                i2 = 0;
                s = 0;
                SS++;
            }
            if (SS == 60) {
                SS = 0;
                MM++;
            }
            if (MM == 60) {
                MM = 0;
                HH++;
            }
            if (HH < 10) sHH = "0" + HH;
            else sHH = HH + "";
            if (MM < 10) sMM = "0" + MM;
            else sMM = MM + "";
            if (SS < 10) sSS = "0" + SS;
            else sSS = SS + "";
            Message msg = new Message();
            msg.what = 2;
            msg.obj = sHH + ":" + sMM + ":" + sSS + ":" + s;
            handler.sendMessage(msg);
            startRunOnUI();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void startAsyncTask(int inputValue) {
        AsyncTask<Integer, Integer, Boolean> task =
                new AsyncTask<Integer, Integer, Boolean>() {
                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        textTimer.setTextColor(Color.BLUE);

                    }

                    @Override
                    protected Boolean doInBackground(Integer... params) {
                        for (i3 = 1; i3 < params[0] + 1; i3++) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            if (i3 < params[0] + 1) {
                                final Integer[] values = {i3};
                                this.publishProgress(values);
                            }
                        }
                        return true;
                    }

                    @Override
                    protected void onProgressUpdate(Integer... values) {
                        super.onProgressUpdate(values);
                        progressBar.setProgress(values[0]);
                        String remain = (asyncValue - values[0]) + "";
                        edtTime.setText(remain);
                    }

                    @Override
                    protected void onPostExecute(Boolean aBoolean) {
                        super.onPostExecute(aBoolean);
                        textTimer.setTextColor(Color.RED);
                        Stop();
                        postNotify();
                    }
                };
        task.execute(new Integer[]{inputValue});
    }

    public void linkViewAndSetListener() {
        btnStart = (Button) findViewById(R.id.btn_start);
        btnStart.setOnClickListener(this);
        btnStop = (Button) findViewById(R.id.btn_stop);
        btnStop.setOnClickListener(this);
        btnContinue = (Button) findViewById(R.id.btn_continue);
        btnContinue.setOnClickListener(this);
        textTimer = (TextView) findViewById(R.id.text_timer);
        textRunOnUI = (TextView) findViewById(R.id.text_runOnUI);
        textStatus = (TextView) findViewById(R.id.text_status);
        edtTime = (EditText) findViewById(R.id.edit_time);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        image = (ImageView) findViewById(R.id.image1);
    }
}
